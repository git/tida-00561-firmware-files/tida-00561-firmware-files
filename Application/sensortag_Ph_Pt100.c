/**************************************************************************************************
  Filename:       SensorTag_Ph_Pt100.c


  Description:    This file contains the Sensor Tag sample application,
                  pH and PT100 part, for use with the TI Bluetooth Low 
                  Energy Protocol Stack.

 Group: WCS, BTS
 Target Device: CC2650, CC2640, CC1350

 ******************************************************************************
 
* Copyright (C) 2016 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *

 ******************************************************************************
 Release Name: ble_sdk_2_02_01_18
 Release Date: 2016-10-26 15:20:04
 *****************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include <xdc/runtime/Log.h>   // For Log_warning1("Warning number #%d", 4); things
#include <xdc/runtime/Diags.h> // For Log_print0(Diags_USER1, "hello"); things.

#include "gatt.h"
#include "gattservapp.h"


#include "ph_pt100service.h"
#include "sensortag_Ph_Pt100.h"
#include "sensor_LMP91200_ADS1120.h"
#include "SensorUtil.h"
#include "board.h"

#include "string.h"
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Task.h>

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

// How often to perform sensor reads (milliseconds)
#define SENSOR_DEFAULT_PERIOD   1000

// Delay from sensor enable to reading measurememt 
#define TEMP_MEAS_DELAY         200 

// Length of the data for this sensor
#define SENSOR_DATA_LEN         PH_PT100_DATA_LEN

// Event flag for this sensor
#define SENSOR_EVT              TI_PH_ADC_SENSOR_EVT

// Task configuration
#define SENSOR_TASK_PRIORITY    1
#define SENSOR_TASK_STACK_SIZE  700

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */
// Entity ID used to check for source and/or destination of messages
static ICall_EntityID sensorSelfEntity;

// Semaphore used to post events to the application thread
static ICall_Semaphore sensorSem;

// Task setup
static Task_Struct sensorTask;
static Char sensorTaskStack[SENSOR_TASK_STACK_SIZE];

// Parameters
static uint8_t sensorConfig;
//todo static uint16_t sensorPeriod;

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void sensorTaskFxn(UArg a0, UArg a1);
static void sensorConfigChangeCB(uint8_t paramID);
static void initCharacteristicValue(uint8_t paramID, uint8_t value,
                                    uint8_t paramLen);

/*********************************************************************
 * PROFILE CALLBACKS
 */
static sensorCBs_t sensorCallbacks =
{
  sensorConfigChangeCB,  // Characteristic value change callback
};


/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn       SensorTagPh_Pt100_createTask
 *
 * @brief   Task creation function for the SensorTag
 *
 * @param   none
 *
 * @return  none
 */
void SensorTagPh_Pt100_createTask(void)
{
  Task_Params taskParames;
  
  // Create the task for the state machine
  Task_Params_init(&taskParames);
  taskParames.stack = sensorTaskStack;
  taskParames.stackSize = SENSOR_TASK_STACK_SIZE;
  taskParames.priority = SENSOR_TASK_PRIORITY;

  Task_construct(&sensorTask, sensorTaskFxn, &taskParames, NULL);
}


/*********************************************************************
 * @fn      SensorTagPh_Pt100_processCharChangeEvt
 *
 * @brief   SensorTag Ph & Pt100 event handling 
 *
 * @param   paramID - identifies the characteristic that was changed
 *
 * @return  none
 *
 */
void SensorTagPh_Pt100_processCharChangeEvt(uint8_t paramID)
{
  uint8_t newValue;

  switch (paramID)
  {
  case SENSOR_CONF:

    if (sensorConfig != ST_CFG_ERROR)
    {
        Ph_Pt100_GetParameter( SENSOR_CONF, &newValue );

      if (newValue == ST_CFG_SENSOR_DISABLE)
      {
        // Reset characteristics
        initCharacteristicValue(SENSOR_DATA, 0, SENSOR_DATA_LEN);

        // Deactivate task
        Task_setPri(Task_handle(&sensorTask), -1);
      }
      else
      {
        Task_setPri(Task_handle(&sensorTask), SENSOR_TASK_PRIORITY);
      }

      sensorConfig = newValue;
    }
    else
    {
      // Make sure the previous characteristics value is restored
      initCharacteristicValue(SENSOR_CONF, sensorConfig, sizeof(uint8_t));
    }
    break;

  case SENSOR_PERI:
    Ph_Pt100_GetParameter( SENSOR_PERI, &newValue );
//todo	sensorPeriod = newValue * SENSOR_PERIOD_RESOLUTION;
    break;

  default:
    // Should not get here
    break;
  }
}

/*********************************************************************
 * @fn      sensorTaskInit
 *
 * @brief   Initialization function for the SensorTag Ph & Pt100 addon board
 *
 * @param   none
 *
 * @return  none
 */
void sensorTaskInit( void )
{
  // Add service
  Ph_Pt100_AddService (GATT_ALL_SERVICES);

  // Register callbacks with profile 
  Ph_Pt100_RegisterAppCBs( &sensorCallbacks );

  // Initialize the module state variables
//todo   sensorPeriod = SENSOR_DEFAULT_PERIOD;

  // Initialize characteristics and sensor driver
  SensorTagPh_Pt100_Reset();
  initCharacteristicValue(SENSOR_PERI,
                          SENSOR_DEFAULT_PERIOD / SENSOR_PERIOD_RESOLUTION,
                          sizeof(uint8_t));
}

/*********************************************************************
 * @fn      SensorTagPh_Pt100_Reset
 *
 * @brief   Reset characteristics
 *
 * @param   none
 *
 * @return  none
 */
void SensorTagPh_Pt100_Reset (void)
{
  sensorConfig = ST_CFG_SENSOR_DISABLE;
  initCharacteristicValue(SENSOR_DATA, 0x56, SENSOR_DATA_LEN );
  initCharacteristicValue(SENSOR_CONF, sensorConfig, sizeof(uint8_t));

  // Initialize the driver
  sensorLMP91200_ADS1120_Initdriver();
}


/*********************************************************************
* Private functions
*/

/*********************************************************************
 * @fn      sensorTaskFxn
 *
 * @brief   The task loop of the PH readout task
 *
 * @param   a0 (not used)
 *
 * @param   a1 (not used)
 *
 * @return  none
 */
static void sensorTaskFxn(UArg a0, UArg a1)
{
  typedef union
  {
    struct
    {
      uint16_t ph,rcal,pt100;
    } v;
    uint16_t a[3];
  } Data_t;

  // Register task with BLE stack
  ICall_registerApp(&sensorSelfEntity, &sensorSem);
  
  // Initialize the task
  sensorTaskInit();
     
  // power-on the board
  sensorLMP91200_ADS1120_Poweronboard();    //gp1: power on the board only once. 

//  long temp = 0x12345678; //deb_gp

  // Task loop
  while (true)
  {
    if (sensorConfig == ST_CFG_SENSOR_ENABLE)
    {
      Data_t data;
  //For power saving purposes we turn on and off the board for each data acquisition
  // the sensorPeriod is supposed to be very long, in the case if the user wants to keep the board on
  // it can be done by moving the Poweronboard() &  sensorLMP91200_ADS1120_Init() to  SensorTagPh_Pt100_Reset () 
  // and only performs the reading in the loop
    

    // Read data from ADC for ph and Pt100
    //gp1 SPI CSN's are set/cleared in the ReadData function
    Log_info0("sensorLMP91200_ADS1120_ReadData");
    sensorLMP91200_ADS1120_ReadData(&data.v.ph, &data.v.rcal ,&data.v.pt100);

//    memcpy(&data.a,&temp,8);
//    temp ++;
    
    // Update GATT
    Ph_Pt100_SetParameter( SENSOR_DATA, SENSOR_DATA_LEN, data.a);
      
     // Next cycle
    DELAY_MS(2000);     //gp1  chnaged from 800
    }
    else
    {
      DELAY_MS(SENSOR_DEFAULT_PERIOD);
    }
  }
}


/*********************************************************************
 * @fn      sensorChangeCB
 *
 * @brief   Callback from Ph & Pt100 Service indicating a value change
 *
 * @param   paramID - parameter ID of the value that was changed.
 *
 * @return  none
 */
static void sensorConfigChangeCB(uint8_t paramID)
{
  // Wake up the application thread
  SensorTag_charValueChangeCB(SERVICE_ID_PH_PT100,paramID);      
}


/*********************************************************************
 * @fn      initCharacteristicValue
 *
 * @brief   Initialize a characteristic value
 *
 * @param   paramID - parameter ID of the value to be initialized
 *
 * @param   value - value to initialize with
 *
 * @param   paramLen - length of the parameter
 *
 * @return  none
 */
static void initCharacteristicValue(uint8_t paramID, uint8_t value,
                                    uint8_t paramLen)
{
  uint8_t data[SENSOR_DATA_LEN];

  memset(data,value,paramLen);
  Ph_Pt100_SetParameter( paramID, paramLen, data);
}


/*********************************************************************
*********************************************************************/

