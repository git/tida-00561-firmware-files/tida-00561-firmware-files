The files in this repository should be added to the standard Sensortag project from the CC2650-ble_sdk_2_02_01_18. 
There will be some duplicates, some new, and some existing that will need to be replaced in the standard STK project.

Build the project with the FLASHROM project Build config. 
Tools used are as defined for this stack release - 

TI ARM Compiler version 5.2.6
XDC Tools 3.32.00.06 
TI-RTOS version 2.20.01.08
CCS6.2.0 or later (CCSv7.2 was used)
No IAR project at the moment. 


