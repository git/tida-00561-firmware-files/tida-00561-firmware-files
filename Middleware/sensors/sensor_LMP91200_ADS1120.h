/**************************************************************************************************
*  Filename:       sensor_LMP91200_ADS1120.h
* 
*
*  Description:    Driver for the Texas Instruments pH and pt100 signal 
*                   conditioning chips on the add-on board
*
* Copyright (c) 2015-2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************************************/
#ifndef SENSOR_PH_PT100_H
#define SENSOR_PH_PT100_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */
#include "stdint.h"
//#include "Board.h"

#include <stdbool.h>  
/*********************************************************************
 * CONSTANTS
 */


/*********************************************************************
 * TYPEDEFS
 */

/* Sensor data size*/ // BUG: when using 4 the Spiread function reads more than 4 and overwrite on memory
#define DATA_SIZE                       16


#define HIGH            1
#define LOW             0
// ADS1120 Initial registers configuration

   
/*********************************************************************
 * FUNCTIONS
 */
void sensorLMP91200_ADS1120_Poweronboard(void); 
bool sensorLMP91200_ADS1120_Poweroffboard(void);
void sensorLMP91200_ADS1120_Initdriver(void);
void sensorLMP91200_ADS1120_Init_Ph(void);
void sensorLMP91200_ADS1120_Init_Pt100(void);
void sensorLMP91200_ADS1120_ReadData(uint16_t *rawPh, uint16_t *rawRcal, uint16_t *rawPt100); 
void sensorLMP91200_ADS1120_AFESelect(bool select); 
void sensorLMP91200_ADS1120_ADCSelect(bool select);
bool sensorLMP91200_ADS1120_Test(void);
void IoPark(void);
void IoRelease(void);


#ifdef __cplusplus
}
#endif

#endif /* SENSOR_PH_PT100_H */
