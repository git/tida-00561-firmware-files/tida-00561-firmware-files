/**************************************************************************************************
*  Filename:       sensor_LMP91200_ADS1120.c
*  
*  Description:    Driver for the Texas Instruments pH and pt100 signal 
*                   conditioning chips on the add-on board

* Copyright (c) 2015-2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************************************/

/* ------------------------------------------------------------------------------------------------
*                                          Includes
* ------------------------------------------------------------------------------------------------
*/

#include "bsp_spi.h"
#include "sensor_LMP91200_ADS1120.h"
#include "SensorUtil.h"
#include "math.h"
#include "board.h"

#include <ti/drivers/pin/PINCC26XX.h> 
#include <ti/drivers/spi/SPICC26XXDMA.h>


/* ------------------------------------------------------------------------------------------------
*                                           Constants
* ------------------------------------------------------------------------------------------------
*/

#define SENSORTAG_CC2650_PHBOARD_POWER_UP Board_DP3
#define SENSORTAG_CC2650_PHBOARD_LMP91200_CSN  Board_DP0
#define SENSORTAG_CC2650_PHBOARD_ADS1120_CSN Board_DP1

//#define IO_PARKING

/* ------------------------------------------------------------------------------------------------
*                                           Local Variables
* ------------------------------------------------------------------------------------------------
*/
static uint8_t config_LMP9100_phconfig[2] = {0x00, 0x00};  // LMP91200 reset (see page 19 of datasheet) MSB LSB 
// check the 2-step measurement of temperature in the LMP91200 datasheet (page 22)
static uint8_t config_LMP9100_pt100_rcal_config[2] ={0xE8,0x80}; // for calibration using the precision 100ohm res;
static uint8_t config_LMP9100_pt100_rtd_config[2] ={0xA8,0x80}; // for the measurement of temperature
static uint8_t buf[DATA_SIZE];


static uint8_t ADS1120_CMD_WREG = 0x43; // Write register command , 4 bytes will be sent from the adress 0 (see datasheet)
static uint8_t ADS1120_CMD_RREG = 0x23; // Read register command , 4 bytes will be sent from the adress 0 (see datasheet)
static uint8_t ADS1120_CMD_START_SYNC= 0x08; // CMD to start the one shot acquisition
static uint8_t ADS1120_CMD_POWERDOWN= 0x02; // CMD to start the one shot acquisition //gp1
static uint8_t ADS1120_CONFIG_REGISTER_PH[4]= {0x04 ,0x00,0x00,0x00}; // adress 0 -> adress 3// PGA of 4
static uint8_t ADS1120_CONFIG_REGISTER_PT100[4]= {0x01 ,0x00,0x00,0x00}; // adress 0 -> adress 3// PGA disabaled


static PIN_Config phboard_gpioconfigTable[] = 
{
 Board_DP3 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL |PIN_DRVSTR_MIN ,
 Board_DP0 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL |PIN_DRVSTR_MIN ,
 Board_DP1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL |PIN_DRVSTR_MIN ,
 
 PIN_TERMINATE
};

static PIN_State phboardPins;
static PIN_Handle hPhboardPins=NULL;


// Table of pins to be "parked" in when no device is selected. Leaving the pin
// floating causes increased power consumption in WinBond W25XC10.
static PIN_Config BoardSpiInitTable[] = {
    Board_SPI0_MOSI | PIN_PULLDOWN,
    Board_SPI0_MISO | PIN_PULLDOWN,
    Board_SPI0_CLK | PIN_PULLDOWN,// exception
    PIN_TERMINATE
};

static PIN_Handle hSpiPin = NULL;
static PIN_State pinState;


/* ------------------------------------------------------------------------------------------------
*                                           Public functions
* -------------------------------------------------------------------------------------------------
*/

void bspSpiFlashSelect(bool select);    //gp1

/*******************************************************************************
 * @fn          IoPark
 *
 * @brief       Park the IO's connected to the flash to avoid floating
 *              lines during standby. This leads to leakage through the flash
*               device. NB! This is a workaround for bug #6424.
 *
 * @return      none
 */
void IoPark(void)
{

  //check to see if GPIO's are mapped - if no then ...
  if (hSpiPin == NULL)
  {
    // Make the SPI lines available for reconfiguration
    bspSpiClose();
    
    // Configure SPI lines as IO and set them according to BoardSpiInitTable
    hSpiPin = PIN_open(&pinState, BoardSpiInitTable);
  }
}
/*******************************************************************************
 * @fn          IoRelease
 *
 * @brief       Reconfigure SPI lines to the ph board for normal opertion
 *
 * @return      none
 */
void IoRelease(void)
{
  //check to see if GPIO's are mapped - if yes then ...
  if (hSpiPin != NULL)
  {
    // Remove IO-confiuration of SPI lines
    PIN_close(hSpiPin);

    // Configure SPI-bus for the phBoard
//    bspSpiOpen(CC2650_SPI1);TODO

    hSpiPin = NULL;
  }
}




/**************************************************************************************************
 * @fn          sensorLMP91200_ADS1120_Initdriver
 *
 * @brief       Initialise the ph board gpios, should be launched with the init
 *              of the task
 *
 * @return      none
 **************************************************************************************************/

void sensorLMP91200_ADS1120_Initdriver(void)
{
  //Open pin structure for use
  hPhboardPins=PIN_open(&phboardPins,phboard_gpioconfigTable);  
     
  // initialize chip select pins
  PIN_setOutputValue(hPhboardPins,SENSORTAG_CC2650_PHBOARD_LMP91200_CSN, false);
  PIN_setOutputValue(hPhboardPins,SENSORTAG_CC2650_PHBOARD_ADS1120_CSN, false);

  //turn off ph board
  PIN_setOutputValue(hPhboardPins,SENSORTAG_CC2650_PHBOARD_POWER_UP, false);

  // de-delect the on-board flash. 
  bspSpiFlashSelect(false); //gp1

  // Open SPI port
  bspSpiOpen();

}  

/**************************************************************************************************
 * @fn          sensorLMP91200_ADS1120_Init_Ph
 *
 * @brief       Initialise  the ADS1120 on the addon chip by 
 *               configuring the different registers for pH measurement 
 *
 * @return      none
 **************************************************************************************************/
void sensorLMP91200_ADS1120_Init_Ph(void)
{   
   sensorLMP91200_ADS1120_ADCSelect(true); 
   
   // Prepare the ADS1120 to write on his configuration registers 
   bspSpiWrite( &ADS1120_CMD_WREG, 1);   
   
   //Send the 4 bytes to configure the registers for ph
   bspSpiWrite( ADS1120_CONFIG_REGISTER_PH, 4);
  
   sensorLMP91200_ADS1120_ADCSelect(false);
   
  
}

/**************************************************************************************************
 * @fn          sensorLMP91200_ADS1120_Init_Pt100
 *
 * @brief       Initialise  the ADS1120 on the addon chip by 
 *               configuring the different registers for temperature
 *              measurement using Pt100
 *
 * @return      none
 **************************************************************************************************/
void sensorLMP91200_ADS1120_Init_Pt100(void)
{ 
  
   sensorLMP91200_ADS1120_ADCSelect(true);
    
   // Prepare the ADS1120 to write on his configuration registers 
    bspSpiWrite( &ADS1120_CMD_WREG, 1);   
   
   //Send the 4 bytes to configure the registers for pt100
    bspSpiWrite( ADS1120_CONFIG_REGISTER_PT100, 4); 
  
   sensorLMP91200_ADS1120_ADCSelect(false);
 
}


/**************************************************************************************************
 * @fn          sensorLMP91200_ADS1120_ReadData
 *
 * @brief       Reading the ADC data from the LMP91200 
 *
 * @param       rawPh - voltage in 16 bit format
 *
 * @param       rawPt100 - voltage in 16 bit format
 *
 * @return      TRUE if valid data
 **************************************************************************************************/

void sensorLMP91200_ADS1120_ReadData(uint16_t *rawPh, uint16_t *rawRcal, uint16_t *rawPt100)
{
  PIN_setOutputValue(hPhboardPins,SENSORTAG_CC2650_PHBOARD_LMP91200_CSN, 1);
  PIN_setOutputValue(hPhboardPins,SENSORTAG_CC2650_PHBOARD_ADS1120_CSN, 1);
  
  // PH ACQUISITION
   sensorLMP91200_ADS1120_ADCSelect(true);
   
  // SEND START/SYNC CMD
   bspSpiWrite(&ADS1120_CMD_START_SYNC, 1); 
   
   DELAY_MS(55);
    
   bspSpiRead(buf,2); 
  
  *rawPh=  buf[0] << 8 | buf[1] ;

   sensorLMP91200_ADS1120_ADCSelect(false);
     
   // PT100 temperature acquisition
   // we start first by measuring voltage across the calibration resistor
   // see page 20 of lmp91200 datasheet for more details on temp calculation
   
   // initialize ADC registers
   sensorLMP91200_ADS1120_Init_Pt100();
   
// change config register in LMP to switch to Rcalibration acquisition
   sensorLMP91200_ADS1120_AFESelect(true);
   
   bspSpiWrite(config_LMP9100_pt100_rcal_config, 2);
   
   DELAY_MS(10);
   
   sensorLMP91200_ADS1120_AFESelect(false);
  
   // PT100 ACQUISITION
    sensorLMP91200_ADS1120_ADCSelect(true);
   
  // SEND START/SYNC CMD
   bspSpiWrite(&ADS1120_CMD_START_SYNC, 1); 
   
   DELAY_MS(55);
   
   // GET data
   bspSpiRead(buf,2);
   bspSpiWrite(&ADS1120_CMD_POWERDOWN, 1); 
   
   *rawRcal=   buf[0] << 8 | buf[1] ;  
   
   sensorLMP91200_ADS1120_ADCSelect(false);
   
   // change config register in LMP to switch to PT100 acquisition
   sensorLMP91200_ADS1120_AFESelect(true);
   
   bspSpiWrite(config_LMP9100_pt100_rtd_config, 2);
   
   DELAY_MS(10);
      
   sensorLMP91200_ADS1120_AFESelect(false);
  
   // PT100 ACQUISITION
   sensorLMP91200_ADS1120_ADCSelect(true);
   
  // SEND START/SYNC CMD
   bspSpiWrite(&ADS1120_CMD_START_SYNC, 1); 
   
   DELAY_MS(55);
   
   // GET data
   bspSpiRead(buf,2);
   
   *rawPt100=   buf[0] << 8 | buf[1] ;  

   bspSpiWrite(&ADS1120_CMD_POWERDOWN, 1); 
      
   sensorLMP91200_ADS1120_ADCSelect(false);
   
//gp1 - switch back to the low-power mode of the AFE - ie PH mode.  
  sensorLMP91200_ADS1120_Init_Ph(); 
  
  sensorLMP91200_ADS1120_AFESelect(true);
   
   // RESET the LMP91200 registers
   bspSpiWrite(config_LMP9100_phconfig, 2);  
    
   DELAY_MS(10);
    
   sensorLMP91200_ADS1120_AFESelect(false);

    PIN_setOutputValue(hPhboardPins,SENSORTAG_CC2650_PHBOARD_LMP91200_CSN, 0);
    PIN_setOutputValue(hPhboardPins,SENSORTAG_CC2650_PHBOARD_ADS1120_CSN, 0);
     
}

/**************************************************************************************************
 * @fn          sensorLMP91200_ADS1120_Poweronboard
 *
 * @brief       Turn on the load switch of the daughter board (addon), the aim is saving power
 *
 * @return      none
 **************************************************************************************************/

void sensorLMP91200_ADS1120_Poweronboard(void)
{
  PIN_setOutputValue(hPhboardPins,SENSORTAG_CC2650_PHBOARD_POWER_UP, 1);
  
  DELAY_MS(5);
  
  //#ifdef IO_PARKING
  /* Make sure SPI is available */
 // IoRelease();
  //#endif
}

/**************************************************************************************************
 * @fn          sensorLMP91200_ADS1120_Poweroffboard
 *
 * @brief       Turn off the load switch of the daughter board (addon), the aim is saving power
 *
 * @return      none
 **************************************************************************************************/

bool sensorLMP91200_ADS1120_Poweroffboard(void)
{

  PIN_setOutputValue(hPhboardPins,SENSORTAG_CC2650_PHBOARD_POWER_UP, 0);
  
  //#ifdef IO_PARKING
  /* Make sure SPI lines have a defined state */
 // IoPark();
//#endif
  
  return true;
}

/**************************************************************************************************
 * @fn          sensorLMP91200_ADS1120_ADCSelect
 *
 * @brief       Select the ADC chip on the addon board
 *
 * @return      none
 **************************************************************************************************/


// SPI for the ADC1120
void sensorLMP91200_ADS1120_ADCSelect(bool select)
{
  if (select == 1)
  {   
    PIN_setOutputValue(hPhboardPins,SENSORTAG_CC2650_PHBOARD_ADS1120_CSN, 0);

  }
  else
  {   
    PIN_setOutputValue(hPhboardPins,SENSORTAG_CC2650_PHBOARD_ADS1120_CSN, 1);
  }
}

/**************************************************************************************************
 * @fn          sensorLMP91200_ADS1120_AFESelect
 *
 * @brief       Select the AFE chip on the addon board
 *
 * @return      none
 **************************************************************************************************/

// SPI for the AFE LMP91200
void sensorLMP91200_ADS1120_AFESelect( bool  select)
{
  if (select == 1)
  {
   
    PIN_setOutputValue(hPhboardPins,SENSORTAG_CC2650_PHBOARD_LMP91200_CSN, 0);
  }
  else
  {
   
    PIN_setOutputValue(hPhboardPins,SENSORTAG_CC2650_PHBOARD_LMP91200_CSN, 1);
  }
}

/**************************************************************************************************
 * @fn          sensorLMP91200_ADS1120_Test
 *
 * @brief       Run a sensor self-test
 *
 * @return      TRUE if passed, FALSE if failed
 **************************************************************************************************/
bool sensorLMP91200_ADS1120_Test(void)
{
  uint8_t i=0;
   
  // Power on the board
  sensorLMP91200_ADS1120_Poweronboard(); 
  
  // Select this sensor on the SPI bus  
  sensorLMP91200_ADS1120_ADCSelect(true);
  
  // Prepare the ADS1120 to write on his configuration registers 
   bspSpiWrite( &ADS1120_CMD_WREG, 1);   
   
  //Send the 4 bytes to configure the registers 
   bspSpiWrite( ADS1120_CONFIG_REGISTER_PH, 4); 
  
 // Prepare the ADS1120 to write back his configuration registers 
   bspSpiWrite( &ADS1120_CMD_RREG, 1);   
   
  //Read back the registers to approve 
   bspSpiRead(buf,4);
   
  // if the ADC doesn't return the same then the communication failed and the board is not connected
   for ( i=0 ; i<4 ; i++ )
   {
     if ( buf[i] != ADS1120_CONFIG_REGISTER_PH[i] )
     { 
      sensorLMP91200_ADS1120_ADCSelect(false);
      // Power off the board
      sensorLMP91200_ADS1120_Poweroffboard();

       return false;
     }
   }
   
  sensorLMP91200_ADS1120_ADCSelect(false);
  // Power off the board
  sensorLMP91200_ADS1120_Poweroffboard();

  return true;
}
